<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique()->nullable();
            $table->string('first_name')->default('');
            $table->string('last_name')->default('');
            $table->string('email')->unique()->nullable();
            $table->string('interest')->nullable();
            $table->string('occupation')->nullable();
            $table->string('password')->default(bcrypt('000000'));
            $table->text('about')->nullable();
            $table->ipAddress('visitor')->nullable();
            $table->unsignedSmallInteger('title')->default(0);
            $table->string('street')->nullable();
            $table->string('zipcode', 10)->nullable();
            $table->string('city', 60)->nullable();
            $table->char('country', 3)->nullable();
            $table->string('phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->string('fax')->nullable();
            $table->string('skype')->nullable();
            $table->string('personal_timezone', 64)->nullable();
            $table->string('date_format', 10)->nullable();
            $table->string('number_format', 10)->nullable();
            $table->timestamp('last_login')->nullable();
            $table->unsignedSmallInteger('login_failures')->default(0);
            $table->unsignedSmallInteger('locked_until')->default(0);
            $table->char('language', 3)->default('fr');
            $table->boolean('is_readonly')->default(false);
            $table->boolean('deleted')->default(false);
            $table->string('confirmation_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::table('posts', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index()->after('type');
        });
        Schema::table('comments', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index()->after('reply');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('users');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
