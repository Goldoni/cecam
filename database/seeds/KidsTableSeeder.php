<?php

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KidsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker= Factory::create();
        
        for ($i=0;$i<10;$i++){
            DB::table('kids')->insert([
                'name' => $faker->name,
                'birth' => $faker->dateTimeThisYear,
                'weight' => $faker->randomFloat(2,0,99),
                'size' => $faker->numberBetween(0,100),
                'gender' => 'M',
                'status_vaccine' => 'YES',
                'vitamins' => 'YES',
                'deworming' => $faker->dateTimeThisYear,
                'allergy' => 'YES',
                'asthmatic' => 'YES',
                'user_id' => $faker->numberBetween(1,50),
                'created_at' => $faker->dateTimeThisMonth,
                'updated_at' => $faker->dateTimeThisMonth,
            ]);
        }
    }
}
