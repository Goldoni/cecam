<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 20; $i++){
            DB::table('comments')->insert([
                'content' => $faker->sentence(100),
                'commentable_type' => 'App\Post',
                'commentable_id' => $faker->numberBetween(0,20),
                'user_id' => $faker->numberBetween(0,20),
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ]);
        }
    }
}
