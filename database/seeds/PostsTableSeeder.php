<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 20; $i++){
            DB::table('posts')->insert([
                'name' => $faker->name,
                'content' => $faker->sentence(100),
                'user_id' => $faker->numberBetween(0,100),
                'category_id' => $faker->numberBetween(0,100),
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ]);
        }
    }
}
