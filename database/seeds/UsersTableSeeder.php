<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $user  = DB::table('users')->insertGetId([
            'username' => 'Goldoni',
            'first_name' => 'Goldoni',
            'last_name' => 'Fouotsa',
            'email' => 'admin@contact.de',
            'country' => 'CM',
            'language' => 'fr',
            'phone1' => $faker->phoneNumber,
            'password' => \Illuminate\Support\Facades\Hash::make('000000'),
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()'),
            'confirmation_token' => null
        ]);
        $u = \App\User::find($user);
        $u->roles()->sync([1, 2, 3, 4, 5]);

        $user = DB::table('users')->insertGetId([
            'username' => 'Sonia',
            'first_name' => 'Sonia',
            'last_name' => 'Zebaze',
            'email' => 'user@contact.de',
            'country' => 'CM',
            'language' => 'fr',
            'phone1' => $faker->phoneNumber,
            'password' => \Illuminate\Support\Facades\Hash::make('000000'),
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()'),
            'confirmation_token' => null
        ]);
        $u = \App\User::find($user);
        $u->roles()->sync([1, 2]);

        for ($i = 0; $i < 20; $i++) {
            $user =DB::table('users')->insertGetId([
                'username' => $faker->userName.$i,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $faker->email,
                'country' => 'CM',
                'language' => 'fr',
                'about' => $faker->sentence(50),
                'occupation' => $faker->jobTitle,
                'city' => $faker->city,
                'street' => $faker->streetName,
                'phone1' => $faker->phoneNumber,
                'password' => \Illuminate\Support\Facades\Hash::make('000000'),
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()'),
                'confirmation_token' => null
            ]);
            $u = \App\User::find($user);
            $u->roles()->sync([1]);
        }
    }
}
