<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Role();
        $user->name = "USER";
        $user->save();

        $doctor = new \App\Role();
        $doctor->name = "DOCTOR";
        $doctor->save();

        $admin = new \App\Role();
        $admin->name = "ADMINISTRATOR";
        $admin->save();

        $board = new \App\Role();
        $board->name = "BOARDS_MANAGER";
        $board->save();

        $board = new \App\Role();
        $board->name = "UPDATE_BOARDS";
        $board->save();

        $board = new \App\Role();
        $board->name = "DELETE_BOARDS";
        $board->save();

        $users = new \App\Role();
        $users->name = "USERS_MANAGER";
        $users->save();

        $users = new \App\Role();
        $users->name = "UPDATE_USERS";
        $users->save();

        $users = new \App\Role();
        $users->name = "DELETE_USERS";
        $users->save();

        $users = new \App\Role();
        $users->name = "DEPUTY";
        $users->save();



    }
}
