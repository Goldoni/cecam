(function ($) {
 "use strict";

 
/*--------------------------
 scrollUp
---------------------------- */	
	$.scrollUp({
        scrollText: '<i class="fa fa-long-arrow-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    }); 
/*----------------------------
 jQuery MeanMenu
------------------------------ */
	jQuery('.mobile-menu-active').meanmenu();	
	
/*----------------------------
 owl active
------------------------------ */  
    /* slider-active  */
	$('.slider-active').owlCarousel({
	smartSpeed:1000,
	nav:false,
	navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
	responsive:{
		0:{
			items:1
		},
		600:{
			items:1
		},
		1000:{
			items:1
		}
}
})
/* blog-active  */
	$('.blog-active').owlCarousel({
	smartSpeed:1000,
	nav:true,
	navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
	responsive:{
		0:{
			items:1
		},
		600:{
			items:1
		},
		1000:{
			items:1
		}
}
})
/* product-active  */
	$('.product-active').owlCarousel({
	smartSpeed:1000,
	nav:true,
	navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
	responsive:{
		0:{
			items:2
		},
		600:{
			items:3
		},
		1000:{
			items:3
		}
}
})

 /*---------------------
 countdown
--------------------- */
	$('[data-countdown]').each(function() {
	  var $this = $(this), finalDate = $(this).data('countdown');
	  $this.countdown(finalDate, function(event) {
		$this.html(event.strftime('<span class="cdown days"><span class="time-count">%-D</span> <p>Days</p></span> <span class="cdown hour"><span class="time-count">%-H</span> <p>Hour</p></span> <span class="cdown minutes"><span class="time-count">%M</span> <p>Min</p></span> <span class="cdown second"> <span><span class="time-count">%S</span> <p>Sec</p></span>'));
	  });
	});
/*----------------------------
 price-slider active
------------------------------ */  
	  $( "#slider-range" ).slider({
	   range: true,
	   min: 40,
	   max: 600,
	   values: [ 0, 600 ],
	   slide: function( event, ui ) {
		$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
	   }
	  });
	  $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
	   " - $" + $( "#slider-range" ).slider( "values", 1 ) );  
	  
	  
	  
	  /*---------------------
// Ajax Contact Form
--------------------- */

$('.cf-msg').hide();
$('form#cf button#submit').on('click', function() {
    var fname = $('#fname').val();
    var subject = $('#subject').val();
    var email = $('#email').val();
    var msg = $('#msg').val();
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!regex.test(email)) {
        alert('Please enter valid email');
        return false;
    }

    fname = $.trim(fname);
    subject = $.trim(subject);
    email = $.trim(email);
    msg = $.trim(msg);

    if (fname != '' && email != '' && msg != '') {
        var values = "fname=" + fname + "&subject=" + subject + "&email=" + email + " &msg=" + msg;
        $.ajax({
            type: "POST",
            url: "mail.php",
            data: values,
            success: function() {
                $('#fname').val('');
                $('#subject').val('');
                $('#email').val('');
                $('#msg').val('');

                $('.cf-msg').fadeIn().html('<div class="alert alert-success"><strong>Success!</strong> Email has been sent successfully.</div>');
                setTimeout(function() {
                    $('.cf-msg').fadeOut('slow');
                }, 4000);
            }
        });
    } else {
        $('.cf-msg').fadeIn().html('<div class="alert alert-danger"><strong>Warning!</strong> Please fillup the informations correctly.</div>')
    }
    return false;
});
	   
 
})(jQuery); 