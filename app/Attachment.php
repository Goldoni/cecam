<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class Attachment extends Model
{
    public $guarded = [];
    public $appends =['url'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function attachable()
    {
        return $this->morphTo();
    }


    /**
     * @param UploadedFile $file
     * @return $this
     */
    public function uploadPostsFile(UploadedFile $file)
    {
        $ds       = DIRECTORY_SEPARATOR;
        $posts   = 'uploads'.$ds.'posts'.$ds.date('Y-m');
        $data = $file->storePubliclyAs($posts,$file->getClientOriginalName(),'public');
        $this->name = basename($data);
        return $this;
    }

    /**
     * @param UploadedFile $file
     * @return $this
     */
    public function uploadUsersFile(UploadedFile $file)
    {
        $ds       = DIRECTORY_SEPARATOR;
        $posts   = 'uploads'.$ds.'users'.$ds.date('Y-m');
        $usersPath = 'uploads'.$ds.'users'.$ds.date('Y-m');
        $data = $file->storePubliclyAs($posts,$file->getClientOriginalName(),'public');
        Storage::disk('public')->makeDirectory($usersPath);
        $this->name = basename($data);
        $manager = new ImageManager(['driver' => 'gd']);
        $manager->make($file->getRealPath())
            ->orientate()
            ->fit(737,800)
            ->save(public_path($usersPath.$ds.'800'.$this->name));
        $manager = new ImageManager(['driver' => 'gd']);
        $manager->make($file->getRealPath())
            ->orientate()
            ->fit(29,29)
            ->save(public_path($usersPath.$ds.'29'.$this->name));
        $manager = new ImageManager(['driver' => 'gd']);
        $manager->make($file->getRealPath())
            ->orientate()
            ->fit(200,150)
            ->save(public_path($usersPath.$ds.'150'.$this->name));
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlAttribute()
    {
        return [
            'posts' =>url('uploads/posts/'.date('Y-m').'/'.$this->name),
            'user' =>
                [
                    'big' => url('uploads/users/'.date('Y-m').'/'.$this->name),
                    'avatar' => url('uploads/users/'.date('Y-m').'/800'.$this->name),
                    'medium' => url('uploads/users/'.date('Y-m').'/150'.$this->name),
                    'small' => url('uploads/users/'.date('Y-m').'/29'.$this->name),
                ]

        ];
       
    }
    public function deleteFileForUser(){
        Storage::disk('public')->delete('uploads/users/'.date('Y-m').'/'.$this->name);
        Storage::disk('public')->delete('uploads/users/'.date('Y-m').'/800'.$this->name);
        Storage::disk('public')->delete('uploads/users/'.date('Y-m').'/150'.$this->name);
        Storage::disk('public')->delete('uploads/users/'.date('Y-m').'/29'.$this->name);
    }
}
