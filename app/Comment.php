<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $guarded = [];
    protected static $commentable_for = ['Post'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    public static function allFor($model,$model_id)
    {
        $comments=[];
        $by_id=[];
        $records = self::where(['commentable_type'=>$model,'commentable_id'=>$model_id])->orderBy('id','asc')->get();
        $records->load('user');
        foreach ($records as $record){
            if($record->reply){
                $by_id[$record->reply]->attributes['replies'][]=$record;
            }else{
                $record->attributes['replies']=[];
                $by_id[$record->id]=$record;
                $comments[]=$record;
            }
        }
        return array_reverse($comments);
    }

    public static function isCommentable($model,$model_id){
        if (!in_array($model,self::$commentable_for)) {
            return false;
        } else {
            $model = "\\App\\$model";
            return $model::where(['id' => $model_id])->exists();
        }
    }

    /**
     * @param $date
     * @return mixed
     */
    public function getCreatedAtAttribute($date)
    {
        Carbon::setLocale(session('locale'));

        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans();
    }


}
