@extends('layouts.default')

@section('title')
    <title>CECAM | HAMBURG</title>
@endsection

@section('content')
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="img/blog/meta/1.png" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">How to Train Your Dog to Wear  </a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>12</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="img/blog/meta/1.png" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">Dog Training Tips - How To Train a Dog</a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>08</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="img/blog/meta/1.png" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">Do You Give Your Outdoor Dog Super </a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>03</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="img/blog/meta/1.png" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">Cat Health Care | Veterinary Advice</a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>06</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="img/blog/meta/1.png" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">Tips & Advice For Dog Lovers</a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>10</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="img/blog/meta/1.png" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">Dog Health Care | Veterinary Advice</a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>18</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="img/blog/meta/1.png" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">10 Tips & Advice For Dog Lovers</a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>18</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
        <div class="blog-wrap home7-blog-wrap mb-70">
            <div class="blog-img overlay">
                <img src="http://lorempixel.com/360/360/nature" alt="" />
                <a href="#"><i class="fa fa-file-image-o"></i></a>
            </div>
            <div class="blog-info pb-30">
                <div class="blog-meta">
                    <span><a href="#"><img src="img/blog/meta/1.png" alt="" /></a></span>
                    <span><a href="#">By Robi</a></span>
                    <span><i class="fa fa-eye"></i></span>
                    <span>255</span>
                    <span><a href="#"><i class="fa fa-heart-o"></i></a></span>
                    <span>155</span>
                    <span><a href="#"><i class="fa fa-comment-o"></i></a></span>
                    <span>50</span>
                </div>
                <h2><a href="#">How to Train Your Dog to Wear</a></h2>
                <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, reque persequeris sea ad. </p>
                        <span class="date">
                            <span>30</span>
                            <span class="month">Nov</span>
                        </span>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
    <div class="col-md-4 col-xs-12">
                <div class="blog-sidebar-area">
                    <div class="bloger-area">
                        <div class="section-title">
                            <h3>CECAM PARTNERS</h3>
                        </div>
                        <div class="recent-post bg-2">
                            <div class="recent-post-items clear mb-30">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="http://lorempixel.com/68/68/abstract/" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                            <div class="recent-post-items clear mb-30">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="http://lorempixel.com/68/68/abstract/" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                            <div class="recent-post-items clear">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="http://lorempixel.com/68/68/abstract/" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blog-categories mtb-60 p-30-10">
                        <div class="section-title">
                            <h3>Blog Categories</h3>
                        </div>
                        <div class="categories-menu">
                            <ul>
                                <li><a href="#">Friendly Pets<span class="pull-right">(09)</span></a></li>
                                <li><a href="#">Breeds of Pets<span class="pull-right">(07)</span></a></li>
                                <li><a href="#">Humters Pets <span class="pull-right">(08)</span></a></li>
                                <li><a href="#">All Kind of Pets<span class="pull-right">(06)</span></a></li>
                                <li><a href="#">Pet health<span class="pull-right">(05)</span></a></li>
                                <li><a href="#">Pet Supplies<span class="pull-right">(09)</span></a></li>
                                <li><a href="#">Friendly Pets<span class="pull-right">(08)</span></a></li>
                                <li><a href="#">Pet Training<span class="pull-right">(05)</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="recent-post-area mtb-60">
                        <div class="section-title">
                            <h3>Recent Post</h3>
                        </div>
                        <div class="recent-post bg-2">
                            <div class="recent-post-items clear mb-30">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="img/post/1.jpg" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                            <div class="recent-post-items clear mb-30">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="img/post/2.jpg" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                            <div class="recent-post-items clear">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="img/post/3.jpg" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="archives">
                        <div class="section-title">
                            <h3>Archives</h3>
                        </div>
                        <div class="categories-menu">
                            <ul>
                                <li><a href="#">November<span class="pull-right">(09)</span></a></li>
                                <li><a href="#">October<span class="pull-right">(07)</span></a></li>
                                <li><a href="#">September <span class="pull-right">(08)</span></a></li>
                                <li><a href="#">August <span class="pull-right">(06)</span></a></li>
                                <li><a href="#">July<span class="pull-right">(05)</span></a></li>
                                <li><a href="#">June<span class="pull-right">(09)</span></a></li>
                                <li><a href="#">May<span class="pull-right">(08)</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tags-area mtb-60">
                        <div class="section-title">
                            <h3>Tag Cloud</h3>
                        </div>
                        <div class="teg-menu p-30-10 bg-2">
                            <a href="#">audio</a>
                            <a href="#">gallery</a>
                            <a href="#">pets</a>
                            <a href="#">Interior pets</a>
                            <a href="#">Vintage</a>
                            <a href="#">birds</a>
                            <a href="#">Plant</a>
                            <a href="#">All Animal </a>
                            <a href="#">Storage</a>
                            <a href="#">photography</a>
                            <a href="#">Animal</a>
                            <a href="#">video</a>
                            <a href="#">dog</a>
                            <a href="#">sports</a>
                            <a href="#">Decor</a>
                            <a href="#">Post</a>
                        </div>
                    </div>                    
                </div>
    </div>
@endsection