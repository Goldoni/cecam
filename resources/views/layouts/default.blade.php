<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>CECAM HH</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/png" href="img/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- meanmenu css -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- owl.carousel.2.0.0-beta.2.4 css -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <!-- font-awesome v4.6.3 css -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="style.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- modernizr css -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<header class="header-area">
    <div class="header-top bg-1 ptb-10">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-9 col-xs-12">
                    <div class="socil-icon text-uppercase"><a href="{{ route('login') }}">Log-in</a><a href="{{ route('register') }}">Sign Up</a>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-3 col-sm-3 col-xs-12">
                     <div class="socil-icon text-uppercase pull-right">
                        <ul>
                            <li><a href="javascript:void(0);"><i class="fa fa-bell-o"></i></a> <span>2</span>
                                <div class="cart-wrap">
                                    <div class="cart-item clear mt-20">
                                    <div class="sub-total clear text-capitalize">
                                        <strong>2</strong>
                                        <span>items</span>
                                        <span class="pull-right total">
                                            <span>Cart Subtotal :</span>
                                            <strong>100.00</strong>
                                        </span>
                                    </div>
                                    <hr>
                                    <div class="sub-total clear text-capitalize">
                                        <strong>2</strong>
                                        <span>items</span>
                                        <span class="pull-right total">
                                            <span>Cart Subtotal :</span>
                                            <strong>100.00</strong>
                                        </span>
                                    </div>
                                    <hr>
                                    <div class="sub-total clear text-capitalize">
                                        <strong>2</strong>
                                        <span>items</span>
                                        <span class="pull-right total">
                                            <span>Cart Subtotal :</span>
                                            <strong>100.00</strong>
                                        </span>
                                    </div>
                                    <hr>
                                    <div class="sub-total clear text-capitalize">
                                        <strong>2</strong>
                                        <span>items</span>
                                        <span class="pull-right total">
                                            <span>Cart Subtotal :</span>
                                            <strong>100.00</strong>
                                        </span>
                                    </div>
                                    <hr>
                                    <div class="link text-center">
                                        <h5>All Notifications</h5>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom-area">
        <div class="container ">
            <div class="row">
                <div class="col-md-3 col-xs-7">
                    <div class="logo mt-20">
                        <a href="index.html">
                            <!--<img src="img/logo/logo.png" alt="" /> -->
                            <h1>CECAM</h1>
                        </a>
                    </div>
                </div>
                <div class="col-md-9  hidden-sm hidden-xs">
                    <div class="mainmenu text-right">
                        <nav>
                            <ul>
                                <li class="active"><a href="{{ route('page1') }}"><i class="fa fa-home" aria-hidden="true"></i> home</a>
                                </li>
                                <li><a href="#">news</a>
                                    <ul>
                                        <li><a href="{{ route('page1') }}">All News</a></li>
                                        <li><a href="{{ route('page1') }}">Archived Posts</a></li>
                                    </ul>
                                </li>                               
                                <li><a href="shop.html">events</a>
                                    <div class="megamenu">
										<span>
											<a class="mega-title" href="#">Shop Layout</a>
											<a href="shop.html">shop full width</a>
											<a href="shop2.html">shop left Sidebar</a>
											<a href="shop3.html">shop right Sidebar</a>
										</span>
										<span>
											<a class="mega-title" href="#">Shop Pages</a>
											<a href="cart.html">Shoping Cart</a>
											<a href="checkout.html">Checkout</a>
											<a href="checkout.html">Checkout</a>
										</span>
										<span>
											<a class="mega-title" href="#">Product type</a>
											<a href="product-details.html">product details </a>
											<a href="product-details-2.html">product details 2</a>
											<a href="shop3.html">shop right Sidebar</a>
										</span>
                                    </div>
                                </li>
                                <li><a href="{{ route('page4') }}">Blog</a></li>
                                <li><a href="{{ route('page2') }}">about</a></li>
                                <li><a href="{{ route('page3') }}">contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-xs-5 hidden-md hidden-lg">
                    <div class="mobile-menu-area">
                        <div class="mobile-menu">
                            <div class="mobile-menu-active">
                                <nav>
                                    <ul>
                                        <li class="active"><a href="{{ route('page1') }}"><i class="fa fa-home" aria-hidden="true"></i> home</a>
                                        </li>
                                        <li><a href="#">news</a>
                                            <ul>
                                                <li><a href="{{ route('page1') }}">All News</a></li>
                                                <li><a href="{{ route('page1') }}">Archived Posts</a></li>
                                            </ul>
                                        </li>                               
                                        <li><a href="shop.html">events</a>
                                            <div class="megamenu">
                                                <span>
                                                    <a class="mega-title" href="#">Shop Layout</a>
                                                    <a href="shop.html">shop full width</a>
                                                    <a href="shop2.html">shop left Sidebar</a>
                                                    <a href="shop3.html">shop right Sidebar</a>
                                                </span>
                                                <span>
                                                    <a class="mega-title" href="#">Shop Pages</a>
                                                    <a href="cart.html">Shoping Cart</a>
                                                    <a href="checkout.html">Checkout</a>
                                                    <a href="checkout.html">Checkout</a>
                                                </span>
                                                <span>
                                                    <a class="mega-title" href="#">Product type</a>
                                                    <a href="product-details.html">product details </a>
                                                    <a href="product-details-2.html">product details 2</a>
                                                    <a href="shop3.html">shop right Sidebar</a>
                                                </span>
                                            </div>
                                        </li>
                                        <li><a href="{{ route('page4') }}">Blog</a></li>
                                        <li><a href="{{ route('page2') }}">about</a></li>
                                        <li><a href="{{ route('page3') }}">contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-end -->
<!-- slider start -->
<div class="slider-area slide-img-1">
    <div class="container">
        <div class="slider-wrap overlay">
            <div class="row">
                <div class="col-md-12">
                    <div class="slider-active">
                        <div class="single-slider text-center ptb-30">
                            <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, quidem ab !<br /> Lorem ipsum dolor </h1>
                            <p class="text-justify">Join them as they travel internationally as a "with small dog" family. Taking pet travel beyond the confines of road trips and into a world with fewer borders! Follow their adventures all across the globe, wether sailing, taking</p>
                            <a href="#">Continue Reading</a>
                        </div>
                        <div class="single-slider text-center ptb-30">
                            <h1>California's End of Life Option <br /> Act and How it </h1>
                            <p>Join them as they travel internationally as a "with small dog" family. Taking pet travel beyond the confines of road trips and into a world with fewer borders! Follow their adventures all across the globe, wether sailing, taking</p>
                            <a href="#">Continue Reading</a>
                        </div>
                        <div class="single-slider text-center ptb-30">
                            <h1>California's End of Life Option <br /> Act and How it </h1>
                            <p>Join them as they travel internationally as a "with small dog" family. Taking pet travel beyond the confines of road trips and into a world with fewer borders! Follow their adventures all across the globe, wether sailing, taking</p>
                            <a href="#">Continue Reading</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider end -->
<!-- blog-area start-->
<div class="blog-area mtb-80">
    <div class="container">
        <div class="row">
             @yield('content')
             @yield('sidebar')            
        </div>
    </div>
</div>
<!-- blog-area end-->
<!-- footer-area start-->
<footer class="footer-area bg-1">
    <div class="footer-top ptb-60">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="footer-wrap">
                        <div class="footer-logo mb-20">
                            <!-- <img src="img/logo/logo1.png" alt="" /> -->
                            <h1>CECAM HH</h1>
                        </div>
                        <p>No vel nonumy viderer. Duo pertinax cotidieque at, eum te integre detraxit philosophia, quando dictas mea an. Putent nostrud in per, go on the persequeris sea ad. At mei graeci.ad. At mei graeci.</p>
                        <form action="#">
                            <input type="email" placeholder="Jhonbutler@gmail.com"/>
                            <button>Subscribe Now</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4  col-xs-12">
                    <div class="footer-wrap">
                        <h2 class="footer-title">TOP EVENTS </h2>
                        <div class="single-footer-text">
                            <p><i class="fa fa-twitter"></i>Putent nostrud in per, go on the persequeris slabos sea ad. integre detraxit philosophia go on.</p>
                            <span><i class="fa fa-calendar-plus-o"></i>November 02.2017</span>
                            <span><i class="fa fa-comments-o"></i>10 Comments</span>
                        </div>
                        <div class="single-footer-text mtb-20">
                            <p><i class="fa fa-twitter"></i>Putent nostrud in per, go on the persequeris slabos sea ad. integre detraxit philosophia go on.</p>
                            <span><i class="fa fa-calendar-plus-o"></i>November 02.2017</span>
                            <span><i class="fa fa-comments-o"></i>10 Comments</span>
                        </div>
                        <div class="single-footer-text">
                            <p><i class="fa fa-twitter"></i>Putent nostrud in per, go on the persequeris slabos sea ad. integre detraxit philosophia go on.</p>
                            <span><i class="fa fa-calendar-plus-o"></i>November 02.2017</span>
                            <span><i class="fa fa-comments-o"></i>10 Comments</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4  col-xs-12">
                    <div class="footer-wrap">
                        <h2 class="footer-title">CECAM SPONSORS</h2>
                        <div class="footer-img clear">
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                            <a href="#"><img src="http://lorempixel.com/155/115/sports" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-area ptb-40">
        <div class="container">
            <div class="row">
                <div class=" col-sm-6 col-xs-12">
                    <p>&copy; 2017. All rights reserved. </p>
                </div>
                <div class="col-sm-6 col-xs-12 text-right">
                    <p>Designed & Developed by CECAM-IT</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer-area end-->
<!-- all js here -->
<!-- jquery latest version -->
<script src="js/vendor/jquery-1.12.4.min.js"></script>
<!-- bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- owl.carousel.2.0.0-beta.2.4 css -->
<script src="js/owl.carousel.min.js"></script>
<!-- jquery countdown min js -->
<script src="js/jquery.countdown.min.js"></script>
<!-- meanmenu js -->
<script src="js/jquery.meanmenu.js"></script>
<!-- jquery-ui min js -->
<script src="js/jquery-ui.min.js"></script>
<!-- plugins js -->
<script src="js/plugins.js"></script>
<!-- main js -->
<script src="js/main.js"></script>
</body>
</html>
