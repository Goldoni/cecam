@extends('layouts.default')

@section('title')
    <title>CECAM</title>
@endsection

@section('content')
    <div class="col-md-8 col-xs-12">
		<div class="standard-post-wrap">
			<div class="blog-img overlay">
				<img src="http://lorempixel.com/1140/528/abstract/" alt="" />
				<a href="#"><i class="fa fa-file-image-o"></i></a>
			</div>
			<div class="blog-info pb-30">
				<span class="date hidden-xs">
					<span>09</span>
					<span>Nov</span>
				</span>
				<div class="blog-meta">
					<span><a href="#"><img src="img/blog/meta/1.png" alt="" /></a></span>
					<span><a href="#">By Robi</a></span>
					<span><i class="fa fa-eye"></i></span>
					<span>255</span>
					<span><a href="#"><i class="fa fa-heart-o"></i></a></span>
					<span>155</span>
					<span><a href="#"><i class="fa fa-comment-o"></i></a></span>
					<span>50</span>
				</div>
				<h2><a href="">How to Train Your Dog to Wear a Costume</a></h2>
				<p>Egestas lacinia. Sapien penatibus cubilia  rutrum fusce curabitur felis nulla facilisis nunc nullam ornare augue ultrices. Mauris eleifend augue sapien Egestas lacinia. Sapien penatibus cubilia rutrum fusce curabitur felis nulla facilisis nunc nullam ornare augue ultrices. Mauris eleifend augue sapien. Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?</p>
				<p>Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, imilique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?</p>
				<div class="standard-post-content text-center mb-40">
					<i class="fa fa-quote-left"></i>
					<p>At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="standard-post-img mb-40">
						<img src="http://lorempixel.com/360/480/abstract/" alt="" />
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="standard-post-img mb-40">
						<p>At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, Perhaps what's needed is a shift in attitude. To become stronger and resistant to the tribulations of life, maybe the answer is that we need to become softer not tougher. Maybe what the world needs is more nurture.Perhaps what�s needed is a shift in attitude. To become stronger and resistant to the tribulations of life, maybe the answer is that we need to become softer not tougher. Maybe what the world needs is more nurture. At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque At vero eos  At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiisac cusamus .</p>
					</div>
				</div>
			</div>
			<p>Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, imilique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?</p>
			<div class="tag-area mtb-70">
				<span>Tags : </span>
				<ul>
					<li><a href="#">Audio ,</a></li>
					<li><a href="#">Video ,</a></li>
					<li><a href="#">Gellery </a></li>
				</ul>
			</div>
			<div class="share-icon mb-70">
				<ul>
					<li><a href="#"><i class="fa fa-share-alt"></i></a></li>
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram"></i></a></li>
					<li><a href="#"><i class="fa fa-share"></i></a></li>
				</ul>
			</div>
			<div class="comment-area mb-70">
				<div class="comment-wrap clear  mb-30">
					<div class="comment-img floatleft">
						<a href="#">
							<img src="http://lorempixel.com/118/118/abstract/" alt="" />
						</a>
					</div>
					<div class="comment-text">
						<a href="#">Ann Lornner</a>
						<p>Curiosity remaining own see repulsive household advantage sonsun <br />additions. Supposing exquisite daughters eagerness.</p>
						<span class="pull-left">Nov 07, 2016 </span>
						<span class="pull-right">
							<span><a href="#"><i class="fa fa-heart-o"></i></a></span>
							<span>25</span>
							<span><a href="#"><i class="fa fa-comment-o"></i></a></span>
							<span>20</span>
							<span><a href="#"><i class="fa fa-share"></i></a></span>
						</span>
					</div>
				</div>
				<div class="comment-wrap clear mr-68">
					<div class="comment-img floatleft">
						<a href="#">
							<img src="http://lorempixel.com/118/118/abstract/" alt="" />
						</a>
					</div>
					<div class="comment-text">
						<a href="#">Robi Butler</a>
						<p>Curiosity remaining own see repulsive household advantage sonsun <br />additions. Supposing exquisite daughters eagerness.</p>
						<span class="pull-left">Nov 07, 2016 </span>
						<span class="pull-right">
							<span><a href="#"><i class="fa fa-heart-o"></i></a></span>
							<span>25</span>
							<span><a href="#"><i class="fa fa-comment-o"></i></a></span>
							<span>20</span>
							<span><a href="#"><i class="fa fa-share"></i></a></span>
						</span>
					</div>
				</div>
			</div>
			<div class="comment-area mb-70">
				<div class="comment-wrap clear  mb-30">
					<div class="comment-img floatleft">
						<a href="#">
							<img src="http://lorempixel.com/118/118/abstract/" alt="" />
						</a>
					</div>
					<div class="comment-text">
						<a href="#">Ann Lornner</a>
						<p>Curiosity remaining own see repulsive household advantage sonsun <br />additions. Supposing exquisite daughters eagerness.</p>
						<span class="pull-left">Nov 07, 2016 </span>
						<span class="pull-right">
							<span><a href="#"><i class="fa fa-heart-o"></i></a></span>
							<span>25</span>
							<span><a href="#"><i class="fa fa-comment-o"></i></a></span>
							<span>20</span>
							<span><a href="#"><i class="fa fa-share"></i></a></span>
						</span>
					</div>
				</div>
				<div class="comment-wrap clear mr-68">
					<div class="comment-img floatleft">
						<a href="#">
							<img src="http://lorempixel.com/118/118/abstract/" alt="" />
						</a>
					</div>
					<div class="comment-text">
						<a href="#">Robi Butler</a>
						<p>Curiosity remaining own see repulsive household advantage sonsun <br />additions. Supposing exquisite daughters eagerness.</p>
						<span class="pull-left">Nov 07, 2016 </span>
						<span class="pull-right">
							<span><a href="#"><i class="fa fa-heart-o"></i></a></span>
							<span>25</span>
							<span><a href="#"><i class="fa fa-comment-o"></i></a></span>
							<span>20</span>
							<span><a href="#"><i class="fa fa-share"></i></a></span>
						</span>
					</div>
				</div>
			</div>
			<div class="comment-area mb-70">
				<div class="comment-wrap clear  mb-30">
					<div class="comment-img floatleft">
						<a href="#">
							<img src="http://lorempixel.com/118/118/abstract/" alt="" />
						</a>
					</div>
					<div class="comment-text">
						<a href="#">Ann Lornner</a>
						<p>Curiosity remaining own see repulsive household advantage sonsun <br />additions. Supposing exquisite daughters eagerness.</p>
						<span class="pull-left">Nov 07, 2016 </span>
						<span class="pull-right">
							<span><a href="#"><i class="fa fa-heart-o"></i></a></span>
							<span>25</span>
							<span><a href="#"><i class="fa fa-comment-o"></i></a></span>
							<span>20</span>
							<span><a href="#"><i class="fa fa-share"></i></a></span>
						</span>
					</div>
				</div>
			</div>
			<div class="standard-post-form clear mb-70">
				<form action="#">
					<p><input type="text" placeholder="Your Name"/></p>
					<p><input type="email" placeholder="Your Email"/></p>
					<textarea name="#" id="massage" cols="30" rows="10" placeholder="Message"></textarea>
					<button>Summit</button>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('sidebar')
    <div class="col-md-4 col-xs-12">
                <div class="blog-sidebar-area">
                    <div class="bloger-area">
                        <div class="section-title">
                            <h3>CECAM PARTNERS</h3>
                        </div>
                        <div class="recent-post bg-2">
                            <div class="recent-post-items clear mb-30">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="http://lorempixel.com/68/68/abstract/" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                            <div class="recent-post-items clear mb-30">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="http://lorempixel.com/68/68/abstract/" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                            <div class="recent-post-items clear">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="http://lorempixel.com/68/68/abstract/" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blog-categories mtb-60 p-30-10">
                        <div class="section-title">
                            <h3>Blog Categories</h3>
                        </div>
                        <div class="categories-menu">
                            <ul>
                                <li><a href="#">Friendly Pets<span class="pull-right">(09)</span></a></li>
                                <li><a href="#">Breeds of Pets<span class="pull-right">(07)</span></a></li>
                                <li><a href="#">Humters Pets <span class="pull-right">(08)</span></a></li>
                                <li><a href="#">All Kind of Pets<span class="pull-right">(06)</span></a></li>
                                <li><a href="#">Pet health<span class="pull-right">(05)</span></a></li>
                                <li><a href="#">Pet Supplies<span class="pull-right">(09)</span></a></li>
                                <li><a href="#">Friendly Pets<span class="pull-right">(08)</span></a></li>
                                <li><a href="#">Pet Training<span class="pull-right">(05)</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="recent-post-area mtb-60">
                        <div class="section-title">
                            <h3>Recent Post</h3>
                        </div>
                        <div class="recent-post bg-2">
                            <div class="recent-post-items clear mb-30">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="img/post/1.jpg" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                            <div class="recent-post-items clear mb-30">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="img/post/2.jpg" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                            <div class="recent-post-items clear">
                                <div class="post-img floatleft">
                                    <a href="#">
                                        <img src="img/post/3.jpg" alt="" />
                                    </a>
                                </div>
                                <div class="post-info">
                                    <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                                    <span>January 23, 2017</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="archives">
                        <div class="section-title">
                            <h3>Archives</h3>
                        </div>
                        <div class="categories-menu">
                            <ul>
                                <li><a href="#">November<span class="pull-right">(09)</span></a></li>
                                <li><a href="#">October<span class="pull-right">(07)</span></a></li>
                                <li><a href="#">September <span class="pull-right">(08)</span></a></li>
                                <li><a href="#">August <span class="pull-right">(06)</span></a></li>
                                <li><a href="#">July<span class="pull-right">(05)</span></a></li>
                                <li><a href="#">June<span class="pull-right">(09)</span></a></li>
                                <li><a href="#">May<span class="pull-right">(08)</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tags-area mtb-60">
                        <div class="section-title">
                            <h3>Tag Cloud</h3>
                        </div>
                        <div class="teg-menu p-30-10 bg-2">
                            <a href="#">audio</a>
                            <a href="#">gallery</a>
                            <a href="#">pets</a>
                            <a href="#">Interior pets</a>
                            <a href="#">Vintage</a>
                            <a href="#">birds</a>
                            <a href="#">Plant</a>
                            <a href="#">All Animal </a>
                            <a href="#">Storage</a>
                            <a href="#">photography</a>
                            <a href="#">Animal</a>
                            <a href="#">video</a>
                            <a href="#">dog</a>
                            <a href="#">sports</a>
                            <a href="#">Decor</a>
                            <a href="#">Post</a>
                        </div>
                    </div>                    
                </div>
    </div>
@endsection