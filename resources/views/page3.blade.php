@extends('layouts.default')

@section('title')
    <title>CECAM</title>
@endsection

@section('content')
    <div class="col-md-8 col-xs-12">
        <div class="about-img mb-70">
            <img src="http://lorempixel.com/750/375/sports/" alt="" />
        </div>
        <div class="contact-wrap">
            <h2 class="section-title">Contact Us</h2>
            <p>Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse nihil, flexitarian Truffaut synth art party deep v chillwave. Seitan High Life reprehenderit consectetur cupidatat kogi. Et leggings fanny pack, elit bespoke vinyl art party Pitchfork selfies master . Seitan High Life reprehenderit consectetur cupidatat kogi Seitan High Life reprehenderit consectetur cupidatat kogi.</p>
            <h2 class="section-title">Send A Message</h2>
            <div class="from-style">
                <div class="cf-msg"></div>
                <form action="mail.php" method="post" id="cf">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <input type="text" placeholder="Name" id="fname" name="fname">
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <input type="text" placeholder="Email" id="email" name="email">
                        </div>
                        <div class=" col-sm-4 col-xs-12">
                            <input type="text" placeholder="Subject" id="subject" name="subject">
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea class="contact-textarea" placeholder="Message" id="msg" name="msg"></textarea>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <button class="cont-submit button" id="submit" name="submit">Send A Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
    <div class="col-md-4 col-xs-12">
        <div class="blog-sidebar-area">
            <div class="bloger-area">
                <div class="section-title">
                    <h3>CECAM PARTNERS</h3>
                </div>
                <div class="recent-post bg-2">
                    <div class="recent-post-items clear mb-30">
                        <div class="post-img floatleft">
                            <a href="#">
                                <img src="http://lorempixel.com/68/68/abstract/" alt="" />
                            </a>
                        </div>
                        <div class="post-info">
                            <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                            <span>January 23, 2017</span>
                        </div>
                    </div>
                    <div class="recent-post-items clear mb-30">
                        <div class="post-img floatleft">
                            <a href="#">
                                <img src="http://lorempixel.com/68/68/abstract/" alt="" />
                            </a>
                        </div>
                        <div class="post-info">
                            <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                            <span>January 23, 2017</span>
                        </div>
                    </div>
                    <div class="recent-post-items clear">
                        <div class="post-img floatleft">
                            <a href="#">
                                <img src="http://lorempixel.com/68/68/abstract/" alt="" />
                            </a>
                        </div>
                        <div class="post-info">
                            <a href="#">Etiam dictum. Nunc enim. <br/>tellus, aliquam rhoncus.  </a>
                            <span>January 23, 2017</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="archives">
                <div class="section-title">
                    <h3>Archives</h3>
                </div>
                <div class="categories-menu">
                    <ul>
                        <li><a href="#">November<span class="pull-right">(09)</span></a></li>
                        <li><a href="#">October<span class="pull-right">(07)</span></a></li>
                        <li><a href="#">September <span class="pull-right">(08)</span></a></li>
                        <li><a href="#">August <span class="pull-right">(06)</span></a></li>
                        <li><a href="#">July<span class="pull-right">(05)</span></a></li>
                        <li><a href="#">June<span class="pull-right">(09)</span></a></li>
                        <li><a href="#">May<span class="pull-right">(08)</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection